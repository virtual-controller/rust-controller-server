use anyhow::Result;
use futures::StreamExt;
use libp2p::{
    multiaddr::{Multiaddr, Protocol},
    ping,
    swarm::SwarmEvent,
    identify,
    swarm::NetworkBehaviour,
    StreamProtocol,
    core::muxing::StreamMuxerBox,
};
use std::{collections::HashMap, time::Duration};
use tokio::sync::Mutex;
use std::net::Ipv4Addr;
use tracing::{info, debug, trace, warn};
use std::borrow::Cow;
use futures::AsyncReadExt;
use futures::FutureExt;
use std::sync::Arc;
use std::pin::Pin;
use futures::Future;
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc;
pub use libp2p::PeerId;
use std::fmt;
use rand::thread_rng;
use libp2p::Transport;


const CONTROLLER_PROTOCOL: StreamProtocol = StreamProtocol::new("/controller/0.1.0");

// TODO how to share these with the web app?
#[derive(Serialize, Deserialize, Debug)]
pub struct Libp2pJoystickMessage {
    pub x: i8,
    pub y: i8,
    pub direction: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Mode {
    Down,
    Up,
}

// Implement to_string
impl fmt::Display for Mode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
        // or, alternatively:
        // fmt::Debug::fmt(self, f)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Libp2pButtonMessage {
    pub name: String,
    pub mode: Mode,
}


#[derive(Serialize, Deserialize, Debug)]
pub enum Libp2pMessageType {
    JoystickMessage(Libp2pJoystickMessage),
    ButtonMessage(Libp2pButtonMessage),
    Address(String, String),
    Connected,
    Disconnected,
}

#[derive(Debug)] 
pub struct ControllerMessage {
    pub peer: libp2p::PeerId,
    pub msg: Libp2pMessageType,
}




pub async fn start_server(tx: mpsc::Sender<ControllerMessage>) -> anyhow::Result<()> {
    #[derive(NetworkBehaviour)]
    struct Behaviour {
        ping: ping::Behaviour,
        identify: identify::Behaviour,
        controller: libp2p_stream::Behaviour,
    }
    
    let mut swarm = libp2p::SwarmBuilder::with_new_identity()
        .with_tokio()
        //.with_other_transport(|id_keys| {
        //    Ok(libp2p_webrtc::tokio::Transport::new(
        //        id_keys.clone(),
        //        libp2p_webrtc::tokio::Certificate::generate(&mut thread_rng())?,
        //    )
        //    .map(|(peer_id, conn), _| (peer_id, StreamMuxerBox::new(conn))))
        //})?
        .with_websocket(
            (libp2p::tls::Config::new, libp2p::noise::Config::new),
                libp2p::yamux::Config::default, 
        ).await?
        .with_behaviour(|keypair| {
            let ident = identify::Behaviour::new(identify::Config::new(
                "/controller/id/1.0.0".to_string(),
                keypair.public(),
            ));

            let p = ping::Behaviour::default();

            let s = libp2p_stream::Behaviour::new();

            Behaviour { ping: p, identify: ident, controller: s }
        })?
        .with_swarm_config(|cfg| {
            cfg.with_idle_connection_timeout(
                Duration::from_secs(u64::MAX), // Allows us to observe the pings.
            )
        })
        .build();

    let mut control = swarm.behaviour().controller.new_control();
    let mut incoming_controller_streams = control
        .accept(CONTROLLER_PROTOCOL)
        .unwrap();

    let address_webrtc = Multiaddr::from(Ipv4Addr::UNSPECIFIED)
        .with(Protocol::Udp(0))
        .with(Protocol::WebRTCDirect);

    if swarm.listen_on(address_webrtc.clone()).is_ok() {
        info!("Listening on {:?}", &address_webrtc);
    } else {
        info!("Cannot listen on {:?}", &address_webrtc);
    }

    let wss_endpoint = Cow::from("/test");
    let address_websocket = Multiaddr::from(Ipv4Addr::UNSPECIFIED)
        .with(Protocol::Tcp(0))
        .with(Protocol::Wss(wss_endpoint));

    if swarm.listen_on(address_websocket.clone()).is_ok() {
        info!("Listening on {:?}", &address_websocket);
    } else {
        info!("Cannot listen on {:?}", &address_websocket);
    }
    
    let ws_endpoint = Cow::from("/test2");
    let address_websocket = Multiaddr::from(Ipv4Addr::UNSPECIFIED)
        .with(Protocol::Tcp(0))
        .with(Protocol::Ws(ws_endpoint));

    if swarm.listen_on(address_websocket.clone()).is_ok() {
        info!("Listening on {:?}", &address_websocket);
    } else {
        info!("Cannot listen on {:?}", &address_websocket);
    }
    let address = loop {
        if let SwarmEvent::NewListenAddr { address, .. } = swarm.select_next_some().await {
            if address
                .iter()
                .any(|e| e == Protocol::Ip4(Ipv4Addr::LOCALHOST))
            {
                debug!(
                    "Ignoring localhost address to make sure the example works in Firefox"
                );
                continue;
            }
            
            // TODO how to report multiple addresses?
            info!(%address, "Listening");

            break address;
        }
    };

    let addr = address.with(Protocol::P2p(*swarm.local_peer_id()));

    let ip: std::net::Ipv4Addr = addr.iter().filter(|e| if let libp2p::multiaddr::Protocol::Ip4(_) = e {true} else {false}).map(|e| if let libp2p::multiaddr::Protocol::Ip4(addr) = e {Some(addr)} else {None}).collect::<Vec<_>>().first().unwrap().unwrap();
    debug!("Our libp2p IP is: {:?}", ip);
    let url = format!("http://{}:8080/index.html?addr={}", ip, &addr);

    let code = qrcode::QrCode::with_error_correction_level(&url, qrcode::EcLevel::L).unwrap();

    use qrcode::render::unicode;
    let utf8_code = code.render::<unicode::Dense1x2>()
        .dark_color(unicode::Dense1x2::Light)
        .light_color(unicode::Dense1x2::Dark)
        .build();
    info!("We think our address is: {:?}\n{:?}\n{}", addr, url, utf8_code.clone());

    use qrcode::render::svg;
    let svg_code = code.render::<svg::Color>().build();

    let addr_msg = ControllerMessage { peer: *swarm.local_peer_id(), msg: Libp2pMessageType::Address(url.clone(), svg_code.clone()) };
    tx.send(addr_msg).await.unwrap();

    let streams: Arc<Mutex<HashMap<libp2p::PeerId, (Arc<Mutex<Box<Vec<u8>>>>, Arc<Mutex<libp2p::Stream>>)>>> = Arc::new(Mutex::new(HashMap::new()));
    let mut stream_read_futures = futures::stream::FuturesUnordered::<Pin<Box<dyn Future<Output = _> + Send>>>::new();

    loop {
        let streams_peers = streams.clone();
        let tx_read = tx.clone();

        // XXX FuturesUnordered, when empty, returns None
        // we want to block instead
        let stream_read_futures_future = if stream_read_futures.is_empty() {
            futures::future::Fuse::terminated()
        } else {
            stream_read_futures.next().fuse()
        };

        tokio::select! {
            swarm_event = swarm.next() => {
                let swarm_event = swarm_event.unwrap();
                match swarm_event {
                    libp2p::swarm::SwarmEvent::NewListenAddr { address, .. } => {
                        let listen_address = address.with_p2p(*swarm.local_peer_id()).unwrap();
                        info!("New listen address: {:?}", listen_address);
                },
                    event => trace!(?event),
                }
            },
            Some((peer, stream)) = incoming_controller_streams.next() => {
                info!("Peer connected over controller protocol: {:?}", peer);

                let connected_msg = ControllerMessage { peer, msg: Libp2pMessageType::Connected };
                tx.send(connected_msg).await.unwrap();

                let buf = vec![0u8; 1024];
                let buf = Box::new(buf);
                let buf = Mutex::new(buf);
                let buf = Arc::new(buf);
                // TODO should the stream be the key? but then we cant access the buf when creating
                // the futures
                let stream = Mutex::new(stream);
                let stream = Arc::new(stream);
                let mut streams = streams_peers.lock().await;
                streams.insert(peer.clone(), (buf.clone(), stream.clone()));
                let r2 = async move {
                    //let mut streams2 = streams2.lock().unwrap();
                    //let (buf, stream) = streams2.get_mut(&peer).unwrap();
                    let buf2 = buf.clone();
                    let stream2 = stream.clone();
                    let mut buf = buf.lock().await;
                    let mut stream = stream.lock().await;
                    let r = (stream).read(&mut buf).await;
                    drop(stream);
                    (r, buf2, stream2, peer, 0)
                };
                let r2 = Box::pin(r2);
                stream_read_futures.push(r2);
            },
            idk = stream_read_futures_future => {
                match idk {
                    None => {},
                    Some((size, buf, stream, peer, additional_bytes)) => {
                        let size = if let Ok(size) = size {
                            size + additional_bytes
                        } else {
                            info!("Closing the connection due to read error");
                            let msg = Libp2pMessageType::Disconnected;
                            let msg = ControllerMessage { peer, msg };
                            tx_read.send(msg).await.unwrap();
                            continue;
                        };
                        if size == 0 {
                            info!("Closing the connection due to no data");
                            let msg = Libp2pMessageType::Disconnected;
                            let msg = ControllerMessage { peer, msg };
                            tx_read.send(msg).await.unwrap();
                            continue;
                        }

                        let buf_closure = buf.clone();
                        let mut buf = buf.lock().await;
                        let mut buf_clone = vec![0u8; 1024];
                        buf_clone[..size].clone_from_slice(&buf[..size]);
                        let data_slice = &buf_clone[..size];
                        // TODO how to pass this up? Channel?
                        debug!("Stream data from peer {:?}: {:?};{:?}", peer, size, data_slice);
                        let data = std::str::from_utf8(data_slice).unwrap();
                        debug!("data: {:?}", data);
                        // TODO need to handle getting multiple messages, comma separated?
                        // search for }{ and split that
                        let mut i = 0;
                        while let Some(index) = data[i..].find("}{") {
                            debug!("i: {}; index: {}", i, index);
                            let data = &data[i..i+index+1];
                            debug!("data2: {:?}", data);
                            match serde_json::from_str(data) {
                                Ok(data) => {
                                    let msg = data;
                                    debug!("msg: {:?}", msg);
                                    let msg = ControllerMessage { peer, msg };
                                    tx_read.send(msg).await.unwrap();
                                },
                                // TODO do we want to save this part of the message for later?
                                Err(e) => {
                                    // msg looks like: '10, direction: 'C'}'
                                    warn!("Could not parse message: {:?}", e);
                                },
                            }
                            i = i + index + 1;
                        }
                        // Last iteration
                        debug!("i: {}", i);
                        let data = &data[i..];
                        debug!("datafinal: {:?}", data);
                        let start_index = match serde_json::from_str(data) {
                            Ok(data) => {
                                let msg: Libp2pMessageType = data;
                                debug!("msg: {:?}", msg);
                                let msg = ControllerMessage { peer, msg };
                                tx_read.send(msg).await.unwrap();
                                0
                            },
                            Err(e) => {
                                // msg looks like: '{JoystickMessage'
                                warn!("error parsing message: {:?}", e);
                                // move the data to the begining of the buffer, then start the read
                                // from the slice
                                for (i, item) in data.chars().enumerate(){
                                    buf[i] = item as u8;
                                }
                                data.len()
                            },
                        };

                        let r2 = async move {
                            let buf2 = buf_closure.clone();
                            let stream2 = stream.clone();
                            let mut buf = buf_closure.lock().await;
                            let mut stream = stream.lock().await;
                            let r = (stream).read(&mut buf[start_index..]).await;
                            drop(stream);
                            (r, buf2, stream2, peer, start_index)
                        };
                        let r2 = Box::pin(r2);
                        stream_read_futures.push(r2);
                    },
                };
            },
            _ = tokio::signal::ctrl_c() => {
                info!("Quitting...");
                break;
            },
        }
    }


    Ok(())
}
