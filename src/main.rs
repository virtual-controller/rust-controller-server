use std::collections::HashMap;
use anyhow::Result;
use tokio::sync::mpsc;
use rust_joystick_server::start_server;
use ntex::web;
use ntex_files::NamedFile;
use std::path::{Path, PathBuf};

async fn index(req: web::HttpRequest) -> Result<NamedFile, web::Error> {
    let path: PathBuf = req.match_info().query("filename").parse().unwrap();
    let path = Path::new("./dist").join(path);
    Ok(NamedFile::open(path)?)
}


#[ntex::main]
async fn main() -> anyhow::Result<()> {
    let _ = tracing_subscriber::fmt()
        //.with_env_filter("browser_webrtc_example=debug,libp2p_webrtc=info,libp2p_ping=debug")
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .try_init();
    
    let (tx, mut rx) = mpsc::channel(10_000);
    //let r = start_server(tx).await?;
    let task = start_server(tx);
    let _task = tokio::spawn(task);
    let web_server = web::HttpServer::new(|| web::App::new().route("/{filename}*", web::get().to(index)))
        .bind(("0.0.0.0", 8080))?
        .run();
    ntex::rt::spawn(web_server);

    let mut last_peer_msg = HashMap::new();
    loop {
        let msg = rx.recv().await;
        match msg {
            Some(msg) => {
                println!("msg: {:?}", &msg);
                last_peer_msg.insert(msg.peer, msg.msg);
            },
            None => break,
        }
    }
    Ok(())
}
